import 'package:proyectowordle/proyectowordle.dart';
import 'package:test/test.dart';
import '../bin/proyectowordle.dart';

void main() {
 group('Las letras de juan coinciden?', () {

   test('prueba con juno', () {
     var m =  letrasinicializadas("juan");
     var i = letrasinicializadas("juno");
     expect((coincidencias)(m, i),  letrasinicializadas("ju").toSet());
   });

   test('conciden con jeas', () {
     var m =  letrasinicializadas("juan");
     var i = letrasinicializadas("jeas");
      List<Prueba> x = [];
      x.add(Prueba(numero: 0, letra: "j"));
      x.add(Prueba(numero: 2, letra: "a"));
      expect(coincidencias(m, i), x.toSet());
   });
   test('si es si es joun', () {
      var m = letrasinicializadas("juan");
      var i = letrasinicializadas("joun");
      List<Prueba> x = [];
      x.add(Prueba(numero: 0, letra: "j"));
      x.add(Prueba(numero: 3, letra: "n"));
      expect(coincidencias(m, i), x.toSet());
    });
 });
  group('no hay coincidencias con juan', () {
    test('si es june', () {
      var m = letrasinicializadas("juan");
      var i = letrasinicializadas("june");
      List<Prueba> x = [];
      x.add(Prueba(numero: 2, letra: "a"));
      x.add(Prueba(numero: 3, letra: "n"));
      expect(noCoinciden(m, i), x.toSet());
    });
  });
}
